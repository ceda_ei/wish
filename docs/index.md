# Wish

A customizable bash prompt.

## Features

- Extremely configurable: Wish is configured by a single gINIe file
- Easy to switch themes: Switching themes is as simple as changing a single line.
- Easy custom themes: Users can override colors for plugins.
- Responsive: Adapts to terminal width.
- Easy to create plugins: Plugins are written in bash allowing great flexibility to extend.

## Getting Started

Head over to [Installation](Installation.md)
