# Installation

## One step Installation

```sh
curl https://gitlab.com/ceda_ei/wish/raw/main/install.sh | bash
```

## Manual Installation

+ `cd ~/.config/`
+ `git clone https://gitlab.com/ceda_ei/wish.git`
+ `cp wish/config.default.gie wish/config.gie`

## Customization

Head over to [Customization](Customization.md)
