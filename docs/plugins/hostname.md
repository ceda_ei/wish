# Hostname

Shows the current system's hostname.

## Add to wish

```ini
[plugin]
name = hostname
```

## Config options

No plugin specific config options are available.
