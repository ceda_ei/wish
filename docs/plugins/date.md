# Date

Shows the current date and time.

## Add to wish

```ini
[plugin]
name = date
```

## Config options

| Key    | Meaning                                                                         | Default |
|--------|---------------------------------------------------------------------------------|---------|
| format | set the date format. Refer to `FORMAT` section of  `man date` for more details. | %F %T   |
