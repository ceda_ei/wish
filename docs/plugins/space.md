# Space

Simple plugin that adds an empty space.

## Add to wish

```ini
[plugin]
name = space
```

## Config options

No plugin specific config options are available.
