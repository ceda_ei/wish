# Custom Text

Display the provided custom text in the prompt.


## Add to wish

```ini
[plugin]
name = custom_text
```

## Config options

| Key  | Meaning              | Default |
|------|----------------------|:-------:|
| text | Custom text to show. |    -    |
