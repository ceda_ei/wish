# Battery

Shows the amount of charge in the battery and if it is charging/discharging.

## Add to wish

```ini
[plugin]
name = battery
```

## Config options

| Key         | Meaning                                  | Default |
|-------------|------------------------------------------|---------|
| id          | ID of Battery to check.                  | BAT0    |
| charging    | Symbol while the battery is charging.    | ϟ       |
| discharging | Symbol while the battery is discharging. | ⏚       |
