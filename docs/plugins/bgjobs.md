# Background Jobs

Shows the number of background jobs running

## Add to wish

```ini
[plugin]
name = bg_jobs
```
## Config options

| Key    | Meaning                                     | Default |
|--------|---------------------------------------------|---------|
| suffix | Suffix to add to number of background jobs. | &       |
