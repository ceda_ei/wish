# Python Virtualenv

Shows the currently active virtualenv. If the venv is `.venv`, it shows the parent directory's name.

## Add to wish

```ini
[plugin]
name = python_venv
```

## Config options

No plugin specific config options are available.
