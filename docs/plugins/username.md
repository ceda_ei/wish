# Username

Shows the current unix username

## Add to wish

```ini
[plugin]
name = username
```

## Config options

No plugin specific config options are available.
