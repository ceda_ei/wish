# Newline

Adds a newline to prompt to push the next plugins into next line.

## Add to wish

```ini
[plugin]
name = newline
```

## Config options

No plugin specific config options are available.
