# Path

Shows the current path and whether it is writeable.


## Add to wish

```ini
[plugin]
name = path
```

## Config options

| Key             | Meaning                                      | Default |
|-----------------|----------------------------------------------|---------|
| no_write_suffix | Suffix if the current path is not writeable. |        |
