# Exit Code

Shows the exit code of the last command if it is non-zero.


## Add to wish

```ini
[plugin]
name = exit_code
```

## Config options

No plugin specific config options are available.
