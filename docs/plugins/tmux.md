# Tmux Detached Sessions

Lists the number of tmux sessions that are detached.

### Add to wish
```ini
[plugin]
name = vcs
```
## Config options

| Key    | Meaning                                          | Default |
|--------|--------------------------------------------------|---------|
| suffix | suffix to add after number of detached sessions. | d       |
